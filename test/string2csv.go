package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

var path1 string = "/Users/appleli/work/golang/workspace/test/data2000.txt"
var writePath1 string = "/Users/appleli/work/golang/workspace/test/test200.txt"

func main() {
	fin, err := os.Open(path1) //打开文件
	if err != nil {
		fmt.Println("打开文件出错了", err)
		return
	}

	fou, err := os.Create(writePath1) //创建文件
	if err != nil {
		fmt.Println("打开文件出错了", err)
		return
	}
	defer fin.Close()
	defer fou.Close()
	reader := bufio.NewReader(fin)
	i := 1
	for {
		buf, err := reader.ReadBytes('\n') //读取到文件末尾
		if err != nil && err == io.EOF {
			fmt.Println("读取文件到末尾了")
			break
		} else if err != nil {
			fmt.Println("读取文件出错了")
			break
		}
		str := string(buf)
		strs := strings.Split(str, "|")
		fou.Write([]byte (strs[0] + ","))
		i++
		if i > 1000 {
			break
		}
	}

}
