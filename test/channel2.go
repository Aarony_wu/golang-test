package main

import "fmt"

func main() {
	cha := make(chan int)
	exit := false

	go func() {
		for i := 0; i < 100; i++ {
			cha <- i
			fmt.Println("i的值等于", i)
			if i == 30 {
				exit = true
				return
			}
		}
	}()

	for {
		if exit {
			fmt.Println("结束打印")
			return
		}
		num := <-cha
		fmt.Println("获取到的数据为：", num)
	}

}
