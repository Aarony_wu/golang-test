package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"log"
)

func ExampleClient() {
	client := redis.NewClient(&redis.Options{
		// 替换为实例的连接地址和端口
		Addr: "r-2ze6jzi9b5z5ehrzpb.redis.rds.aliyuncs.com:6379",
		// 替换为实例的密码
		Password: "Br9bfjKThhWXhVZPavGF",
		DB:       0, // use default DB
	})
	// 下述代码为您提供SET与GET的使用示例。
	log.Println("开始获取 get 的数据")
	err := client.Set(context.Background(),"foo", "bar", 0).Err()
	if err != nil {
		log.Println("出错了" + err.Error())
	}

	val, err := client.Get(context.Background(),"foo").Result()
	log.Println("开始获取 get 的数据")
	val, err = client.Get(context.Background(), "app_info_216210247340").Result()
	if err != nil {
		log.Println("出错了" + err.Error())
	}
	fmt.Println("执行完毕", val)
}

func main() {
	ExampleClient()
}
