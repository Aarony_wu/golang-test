package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

// 统计指定目录下，所有.txt文件中，“Love”这个单词 出现的次数
func main() {
	/*var str string = "this is my test,test is "
	strs := strings.Split(str, "test")
	fmt.Println("一共有", len(strs))*/
	fmt.Println("请输入文件夹")
	var path string
	fmt.Scan(&path)
	var count int
	readdir(path, &count)

	fmt.Println("总共有", count, "个数据")
}

func readdir(path string, count *int) {
	f, err := os.OpenFile(path, os.O_RDONLY, os.ModeDir)
	if err != nil {
		fmt.Println("打开文件夹出错了", err)
		return
	}
	//-1 表示读取所有的文件和文件夹
	names, _ := f.ReadDir(-1)
	for _, fname := range names {
		fmt.Println(fname.Name())
		fmt.Println(fname.Info())
		if fname.IsDir() {
			readdir(fname.Name(), count)
		} else {
			*count += readFile(fname.Name())
		}
	}
}

func readFile(path string) int {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("打开文件出错了", err)
	}
	defer f.Close()
	reader := bufio.NewReader(f)
	var count int
	for {
		buf, err := reader.ReadBytes('\n') //读取到文件末尾
		if err != nil && err == io.EOF {
			fmt.Println("读取文件到末尾了")
			break
		} else if err != nil {
			fmt.Println("读取文件出错了")
			break
		}
		str := string(buf)
		strs := strings.Split(str, "Love")
		count = count + len(strs) - 1
	}
	return count
}
