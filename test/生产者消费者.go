package main

import "fmt"

func consumer(ch <-chan int) {
	num := <-ch
	fmt.Println("获取到的数据为", num)
}

func product(ch chan<- int) {
	ch <- 89
	fmt.Println("生产一个数据")
}

func main() {
	cha := make(chan int)
	go product(cha)
	go consumer(cha)
	for {
		;
	}
}
