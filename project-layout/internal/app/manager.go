package app

import (
	log "github.com/sirupsen/logrus"
	"project-layout/internal/app/config"
	_ "project-layout/internal/app/config"
	_ "project-layout/internal/app/log"
)

/*
*
初始化系统配置
*/
func InitSystemConfig() {
	log.Infof("############### 开始加载系统配置################")
	config.NewAppConfigInstance().RegisterChangeEvent(func(config config.AppConfigModel) {
		log.Infof("manager -----注册的事件开始执行了-----")
	})
}
