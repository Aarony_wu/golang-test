package main

import (
	"gorm.io/gen/examples/dal/query"
	model2 "grom-test/gromtool/dao/model"
)

func main() {
	// u refer to query.user
	user := model2.TbSettleSummary{FID: 123}

	u := query.Use(nil).Mytable
	err := u.WithContext(nil).Create(&user) // pass pointer of data to Create

	err // returns error

}
