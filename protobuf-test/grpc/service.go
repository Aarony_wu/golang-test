package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	log.Println("测试 protobuf 的 demo")
	grpcServer := grpc.NewServer()
	RegisterHelloServiceServer(grpcServer, new(HelloServiceImpl))
	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	grpcServer.Serve(lis)

}
