package main

import (
	"fmt"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8080")
	if err != nil {
		fmt.Println("连接错误，", err)
		return
	}
	defer conn.Close()
	fmt.Println("成功与服务端建立连接--------")
	n1, err := conn.Write([]byte("hi ，我是一个测试"))
	if err != nil {
		fmt.Println("发送数据失败了啊", err)
		return
	}
	fmt.Println("发送成功的数据大小为：", n1)
	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("获取数据错误，", err)
		return
	}
	fmt.Println("获取到服务端返回的数据为：", string(buf[:n]))
}
