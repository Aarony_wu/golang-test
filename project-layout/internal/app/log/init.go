package log

import (
	log "github.com/sirupsen/logrus"
	"project-layout/internal/app/config"
)

func init() {
	log.Infof("############### 开始加载日志配置 ################")
	//设置日志格式
	log.SetFormatter(&log.TextFormatter{
		TimestampFormat:           "2006-01-02 15:04:05",
		ForceColors:               true,
		EnvironmentOverrideColors: true,
		FullTimestamp:             true,
		//DisableLevelTruncation:    true,
	})
	//log.SetFormatter(&log.JSONFormatter{}) // 设置 format json
	config.NewAppConfigInstance().RegisterChangeEvent(func(config config.AppConfigModel) {
		log.Infof("日志 -----注册的事件开始执行了-----")
		if config.Log.Level == "debug" {
			log.SetLevel(log.DebugLevel)
		} else if config.Log.Level == "info" {
			log.SetLevel(log.InfoLevel)
		} else if config.Log.Level == "error" {
			log.SetLevel(log.ErrorLevel)
		} else if config.Log.Level == "warn" {
			log.SetLevel(log.WarnLevel)
		}
	})
}
