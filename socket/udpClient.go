package main

import (
	"fmt"
	"net"
)

func readServerMsg(conn net.Conn, buf []byte) {
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("获取到服务器的数据为：", err)
		return
	}
	fmt.Println("获取到服务器的响应为：", string(buf[:n]))
}
func main() {
	conn, err := net.Dial("udp", "127.0.0.1:8001")
	if err != nil {
		fmt.Println("创建客户端连接失败", err)
		return
	}
	fmt.Println("创建客户端连接成功了---------")
	buf := make([]byte, 4096)
	defer conn.Close()
	for i := 0; i < 10; i++ {
		_, err := conn.Write([]byte("这个是我的一个测试" + string(i)))
		if err != nil {
			fmt.Println("数据写出失败", err)
			break
		}
		fmt.Println("往服务器发送数据成功了------")
		go readServerMsg(conn, buf)
	}
	for {

	}

}
