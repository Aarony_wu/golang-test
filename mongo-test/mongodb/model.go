package mongodb

type Sb_sbb_zb struct {
	Id      string  `json:"id"`
	Xse     float64 `json:"xse"`
	Yshwxse float64 `json:"yshwxse"`
	Yslwxse float64 `json:"yslwxse"`
	Xxse    float64 `json:"xxse"`
	Jxse    float64 `json:"jxse"`
	Qcwjsw  float64 `json:"qcwjsw"`
	Yptse   float64 `json:"yptse"`
	Sbflzl1 struct {
		Zsxm     string `json:"zsxm"`
		Zsxmjcxx string `json:"zsxmjcxx"`
	} `json:"sbflzl1"`
	Sbflzl2 struct {
		Zsxm     string `json:"zsxm"`
		Zsxmjcxx string `json:"zsxmjcxx"`
	} `json:"sbflzl2"`
	Sbflzl3 struct {
		Zsxm     string `json:"zsxm"`
		Zsxmjcxx string `json:"zsxmjcxx"`
	} `json:"sbflzl3"`
	Jszmxx []struct {
		Jzmxxuuid string `json:"jzmxxuuid"`
	} `json:"jszmxx"`
}

type Sb_sbb_fb struct {
	Sbuuid     string  `json:"sbuuid"`
	Zsxmdm     string  `json:"zsxmdm"`
	Zsxmzpxse  float64 `json:"zsxmzpxse"`
	Zsxmzpxxse float64 `json:"zsxmzpxxse"`
	Mdtsxx     []struct {
		Tssx string  `json:"tssx"`
		Tsse float64 `json:"tsse"`
	} `json:"mdtsxx"`
}

type Sb_sbjz_flb struct {
	Sbuuid   string `json:"sbuuid"`
	Zsxmxx   string `json:"zsxmxx"`
	Flzluuid string `json:"flzluuid"`
	Jzkmdm   string `json:"jzkmdm"`
	Jzkmmc   string `json:"jzkmmc"`
}

type Sb_sbjz_mxb struct {
	Sbuuid   string `json:"sbuuid"`
	Fphm     string `json:"fphm"`
	Xh       int    `json:"xh"`
	Kprq     string `json:"kprq"`
	FppzDm   string `json:"fppz_dm"`
	TdyslxDm string `json:"tdyslx_dm"`
}
