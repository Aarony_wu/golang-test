package main

import (
	"gorm.io/driver/mysql"
	"gorm.io/gen"
	"gorm.io/gorm"
	"log"
)

func main() {
	//https://gorm.io/zh_CN/gen/database_to_structs.html 官方参考地址
	g := gen.NewGenerator(gen.Config{
		OutPath: "./query/test1",
		//Mode:    gen.WithoutContext | gen.WithDefaultQuery | gen.WithQueryInterface, // 生成模式
		Mode: gen.WithoutContext, // 生成模式
		/*// 如果你希望为可为null的字段生成属性为指针类型, 设置 FieldNullable 为 true
		FieldNullable: true,
		// 如果你希望在 `Create` API 中为字段分配默认值, 设置 FieldCoverable 为 true, 参考: https://gorm.io/docs/create.html#Default-Values
		FieldCoverable: true,
		// 如果你希望生成无符号整数类型字段, 设置 FieldSignable 为 true
		FieldSignable: true,
		// 如果你希望从数据库生成索引标记, 设置 FieldWithIndexTag 为 true
		FieldWithIndexTag: true,
		// 如果你希望从数据库生成类型标记, 设置 FieldWithTypeTag 为 true
		FieldWithTypeTag: true,
		// 如果你需要对查询代码进行单元测试, 设置 WithUnitTest 为 true
		WithUnitTest: true,*/
		//WithUnitTest: true,
	})

	gormdb, _ := gorm.Open(mysql.Open("root:wucf@819@tcp(127.0.0.1:3306)/db_coral_manager?charset=utf8mb4&parseTime=true"))
	g.UseDB(gormdb) // reuse your gorm db

	// 按照约定为结构体 `model.User` 生成类型安全的 DAO API
	g.ApplyBasic(
		// 根据 `user` 表生成结构 `User`
		//g.GenerateModel("user"),
		//g.GenerateModel("tb_coin_issue_retract_record", gen.FieldTrimPrefix("f_")),
		g.GenerateModel("tb_coin_issue_retract_record"),

		// 根据 `user` 表生成结构 `Employee`
		//g.GenerateModelAs("users", "Employee"),
		// 根据 `user` 表和生成时选项生成结构 `User`
		//g.GenerateModel("users", gen.FieldIgnore("address"), gen.FieldType("id", "int64")),
	)

	//g.ApplyBasic(
	// 从当前数据库中生成所有表的结构
	//	g.GenerateAllTable()...,
	//)

	// 生成代码
	g.Execute()
	log.Println("执行完毕")
}
