package config

import (
	log "github.com/sirupsen/logrus"
)

/*
*
初始化配置
*/
func init() {
	log.Infof("############### 开始加载配置文件 ################")
	NewAppConfigInstance().initConfig()
}
