package main

import (
	"fmt"
	"strconv"
)

func main() {
	//浮点数格式化
	//第二个参数f表示正常输出，e/E表示科学计数法，g/G表示保留结果的总位数（整数部分位数+小数部分位数）
	//第三个参数表示保留小数点的位数
	//第四个参数表示使用32位还是64位浮点数来保存结果
	//func FormatFloat(f float64, fmt byte, prec, bitSize int) string
	fmt.Println(strconv.FormatFloat(123.456789, 'f', 4, 64)) //123.4568
	fmt.Println(strconv.FormatFloat(123.456789, 'E', 4, 64)) //1.2346E+02
	fmt.Println(strconv.FormatFloat(123.456789, 'e', 4, 64)) //1.2346e+02
	fmt.Println(strconv.FormatFloat(123.456789, 'g', 4, 64)) //123.5
	fmt.Println(strconv.FormatFloat(123.456789, 'f', 2, 64)) //123.46
}
