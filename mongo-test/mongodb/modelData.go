package mongodb

type BigData struct {
	YjxxGrid struct {
		YjxxGridlb []interface{} `json:"yjxxGridlb"`
	} `json:"yjxxGrid"`
	Xmlbh          string `json:"xmlbh"`
	Xmlmc          string `json:"xmlmc"`
	Sbuuid          string `json:"sbuuid"`
	SBSBbcTjqtxxVO struct {
		Qzdbz       string `json:"qzdbz"`
		Sfyczb      string `json:"sfyczb"`
		Djxh        string `json:"djxh"`
		HyDm        string `json:"hyDm"`
		Zxbztzsuuid string `json:"zxbztzsuuid"`
		ScenceCs    string `json:"scenceCs"`
	} `json:"SBSBbcTjqtxxVO"`
	Bbh      string `json:"bbh"`
	SbxxGrid struct {
		SbxxGridlb []interface{} `json:"sbxxGridlb"`
	} `json:"sbxxGrid"`
	SBzzsybnsrldxxVO struct {
		Sqldpzxh string `json:"sqldpzxh"`
	} `json:"SBzzsybnsrldxxVO"`
	ZzsybsbSbbdxxVO struct {
		ZzsybnsrsbCbfhdccpzzsjxsejsb struct {
			ZzsybnsrsbCbfhdccpzzsjxsejsbbwGrid struct {
				ZzsybnsrsbCbfhdccpzzsjxsejsbbwGridlbVO []struct {
					Xh            int     `json:"xh"`
					Kcl           float64 `json:"kcl"`
					Dqyxdkncpjxse float64 `json:"dqyxdkncpjxse"`
					Cpmc1         string  `json:"cpmc1"`
					Ncphyl        float64 `json:"ncphyl"`
					Dqzyywcb      float64 `json:"dqzyywcb"`
				} `json:"zzsybnsrsb_cbfhdccpzzsjxsejsbbwGridlbVO"`
			} `json:"zzsybnsrsb_cbfhdccpzzsjxsejsbbwGrid"`
			SbbheadVO struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbheadVO"`
		} `json:"zzsybnsrsb_cbfhdccpzzsjxsejsb"`
		ZzsybnsrsbNcphdkczzsjxsejsb struct {
			SbbheadVO struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbheadVO"`
			NcphdkczzsjxsejsbGrid struct {
				NcphdkczzsjxsejsbGridlbVO []struct {
					SchwTrccfBz   string  `json:"schwTrccfBz"`
					SchwCbfJxse   float64 `json:"schwCbfJxse"`
					ScjyJxse      float64 `json:"scjyJxse"`
					ScjyBz        string  `json:"scjyBz"`
					SchwTrccfJxse float64 `json:"schwTrccfJxse"`
					SchwCbfBz     string  `json:"schwCbfBz"`
					ZjxsJxse      float64 `json:"zjxsJxse"`
					ZjxsBz        string  `json:"zjxsBz"`
				} `json:"ncphdkczzsjxsejsbGridlbVO"`
			} `json:"ncphdkczzsjxsejsbGrid"`
		} `json:"zzsybnsrsb_ncphdkczzsjxsejsb"`
		Zzssyyybnsr03Ysfwkcxmmx struct {
			YsfwkcxmmxGrid struct {
				YsfwkcxmmxGridlbVO []struct {
					Bqfse    float64 `json:"bqfse"`
					Bqsjkcje float64 `json:"bqsjkcje"`
					Msxse    float64 `json:"msxse"`
					Qcye     float64 `json:"qcye"`
					Bqykcje  float64 `json:"bqykcje"`
					Ewbhxh   int     `json:"ewbhxh"`
					Qmye     float64 `json:"qmye"`
					Hmc      string  `json:"hmc"`
				} `json:"ysfwkcxmmxGridlbVO"`
			} `json:"ysfwkcxmmxGrid"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr03_ysfwkcxmmx"`
		ZzsybnsrsbJyzyxsyphzb struct {
			JyzyxsyphzbGrid struct {
				JyzyxsyphzbGridlbVO []struct {
					XsjeTgjyjBys  float64 `json:"xsjeTgjyjBys"`
					YsxsslBys     float64 `json:"ysxsslBys"`
					YkcylDkBys    float64 `json:"ykcylDkBys"`
					CyslBys       float64 `json:"cyslBys"`
					XsjeTgjyjLjs  float64 `json:"xsjeTgjyjLjs"`
					YsxsslLjs     float64 `json:"ysxsslLjs"`
					CyslLjs       float64 `json:"cyslLjs"`
					XsjeBtgjyjBys float64 `json:"xsjeBtgjyjBys"`
					XsjeBtgjyjLjs float64 `json:"xsjeBtgjyjLjs"`
					YkcylDcBys    float64 `json:"ykcylDcBys"`
					YkcylZyBys    float64 `json:"ykcylZyBys"`
					YkcylDkLjs    float64 `json:"ykcylDkLjs"`
					YkcylJcLjs    float64 `json:"ykcylJcLjs"`
					YkcylJcBys    float64 `json:"ykcylJcBys"`
					YkcylDcLjs    float64 `json:"ykcylDcLjs"`
					YkcylZyLjs    float64 `json:"ykcylZyLjs"`
				} `json:"jyzyxsyphzbGridlbVO"`
			} `json:"jyzyxsyphzbGrid"`
			SbbheadVO struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbheadVO"`
		} `json:"zzsybnsrsb_jyzyxsyphzb"`
		Zzssyyybnsr04Bqjxsemxb struct {
			BqjxsemxbGrid struct {
				BqjxsemxbGridlbVO []struct {
					Bqfse        float64 `json:"bqfse"`
					Qcye         float64 `json:"qcye"`
					Bqsjdjse     float64 `json:"bqsjdjse,omitempty"`
					Bqydjse      float64 `json:"bqydjse,omitempty"`
					Ewbhxh       int     `json:"ewbhxh"`
					Qmye         float64 `json:"qmye"`
					Hmc          string  `json:"hmc"`
					Bqzce        float64 `json:"bqzce,omitempty"`
					Bqkjjdkjxse  float64 `json:"bqkjjdkjxse,omitempty"`
					Bqsjjjdkjxse float64 `json:"bqsjjjdkjxse,omitempty"`
				} `json:"bqjxsemxbGridlbVO"`
			} `json:"bqjxsemxbGrid"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr04_bqjxsemxb"`
		ZzssyyybnsrCpygxcqkmxb struct {
			SbbheadVO struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbheadVO"`
			CpygxcqkmxbGrid struct {
				CpygxcqkmxbGridlbVO []struct {
					BqrkDcsl      float64 `json:"bqrkDcsl"`
					BqrkDcje      float64 `json:"bqrkDcje"`
					BqrkZgsl      float64 `json:"bqrkZgsl"`
					BqckFysbfZyje float64 `json:"bqckFysbfZyje"`
					BqckYsbfsl    float64 `json:"bqckYsbfsl"`
					BqckFysbfZysl float64 `json:"bqckFysbfZysl"`
					QckcZgsl      float64 `json:"qckcZgsl"`
					BqckYsbfje    float64 `json:"bqckYsbfje"`
					QckcDcje      float64 `json:"qckcDcje"`
					QckcZgje      float64 `json:"qckcZgje"`
					QckcDcsl      float64 `json:"qckcDcsl"`
					BqckFysbfDcje float64 `json:"bqckFysbfDcje"`
					QmkcDcje      float64 `json:"qmkcDcje"`
					BqckFysbfDcsl float64 `json:"bqckFysbfDcsl"`
					BqrkZgje      float64 `json:"bqrkZgje"`
					QmkcDcsl      float64 `json:"qmkcDcsl"`
					QmkcZgsl      float64 `json:"qmkcZgsl"`
					Ewbhxh        int     `json:"ewbhxh"`
					QmkcZgje      float64 `json:"qmkcZgje"`
					Ypxh          string  `json:"ypxh"`
				} `json:"cpygxcqkmxbGridlbVO"`
			} `json:"cpygxcqkmxbGrid"`
		} `json:"zzssyyybnsr_cpygxcqkmxb"`
		ZzssyyybnsrZb struct {
			SlxxForm struct {
				Dlrdz        string `json:"dlrdz"`
				Sfdlsb       string `json:"sfdlsb"`
				Dlrmc        string `json:"dlrmc"`
				DlrsfzjzlDm1 string `json:"dlrsfzjzlDm1"`
				Sqr          string `json:"sqr"`
				Dlrsfzjhm1   string `json:"dlrsfzjhm1"`
				Slr          string `json:"slr"`
				Smr          string `json:"smr"`
				Slrq         string `json:"slrq"`
				Slswjg       string `json:"slswjg"`
			} `json:"slxxForm"`
			ZbGrid struct {
				ZbGridlbVO []struct {
					Mshwxse        float64 `json:"mshwxse"`
					Sqldse         float64 `json:"sqldse"`
					Ckkjzyjksyjse  float64 `json:"ckkjzyjksyjse"`
					Yshwxse        float64 `json:"yshwxse"`
					Asysljsxse     float64 `json:"asysljsxse"`
					Lmc            string  `json:"lmc"`
					Ydksehj        float64 `json:"ydksehj"`
					Ynsejze        float64 `json:"ynsejze"`
					Bqjnqjse       float64 `json:"bqjnqjse"`
					Jzjtsjtse      float64 `json:"jzjtsjtse"`
					Qcwjse         float64 `json:"qcwjse"`
					Qmwjse         float64 `json:"qmwjse"`
					Smrxm          string  `json:"smrxm"`
					JybfYnse       float64 `json:"jybfYnse"`
					QmwjseQjse     float64 `json:"qmwjseQjse"`
					Jxse           float64 `json:"jxse"`
					Msxse          float64 `json:"msxse"`
					Qcwjcbse       float64 `json:"qcwjcbse"`
					Bqybtsejyfj    float64 `json:"bqybtsejyfj"`
					Sqr            string  `json:"sqr"`
					Bqrkcbse       float64 `json:"bqrkcbse"`
					Mslwxse        float64 `json:"mslwxse"`
					Ynsehj         float64 `json:"ynsehj"`
					Bqyjse         float64 `json:"bqyjse"`
					Fcyjse         float64 `json:"fcyjse"`
					JybfNsjctzxse  float64 `json:"jybfNsjctzxse"`
					Mdtbfckxse     float64 `json:"mdtbfckxse"`
					Ynse           float64 `json:"ynse"`
					Qmldse         float64 `json:"qmldse"`
					Qmwjcbse       float64 `json:"qmwjcbse"`
					JybfNsjcybjse  float64 `json:"jybfNsjcybjse"`
					Bqybtse        float64 `json:"bqybtse"`
					Bqybtsecjs     float64 `json:"bqybtsecjs"`
					Xxse           float64 `json:"xxse"`
					SyslNsjctzxse  float64 `json:"syslNsjctzxse"`
					Mdtytse        float64 `json:"mdtytse"`
					SyslNsjcybjse  float64 `json:"syslNsjcybjse"`
					Bqjnsqynse     float64 `json:"bqjnsqynse"`
					Sjdkse         float64 `json:"sjdkse"`
					Ssckkjzyjkstse float64 `json:"ssckkjzyjkstse"`
					Bqybtsedfjyfj  float64 `json:"bqybtsedfjyfj"`
					Ewblxh         int     `json:"ewblxh"`
					Slr            string  `json:"slr"`
					Ajybfjsxse     float64 `json:"ajybfjsxse"`
					Jxsezc         float64 `json:"jxsezc"`
					Yslwxse        float64 `json:"yslwxse"`
				} `json:"zbGridlbVO"`
			} `json:"zbGrid"`
			Sbbhead struct {
				Sbrq1   string `json:"sbrq1"`
				Nsrsbh  string `json:"nsrsbh"`
				Skssqz  string `json:"skssqz"`
				Nsrmc   string `json:"nsrmc"`
				SbsxDm1 string `json:"sbsxDm1"`
				Skssqq  string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr_zb"`
		ZzssyyybnsrHznsqyzzsfpb struct {
			HznsqyzzsfpbGrid struct {
				HznsqyzzsfpbGridlbVO []struct {
					Fzjgfpse            float64 `json:"fzjgfpse"`
					Fzjgnsrsbh          string  `json:"fzjgnsrsbh"`
					Fzjgysfwjzjtfpbl    float64 `json:"fzjgysfwjzjtfpbl"`
					Fzjgybhwjlwjzjtfpse float64 `json:"fzjgybhwjlwjzjtfpse"`
					Fzjgysfwjzjtxssr    float64 `json:"fzjgysfwjzjtxssr"`
					Fzjgybhwjlwfpbl     float64 `json:"fzjgybhwjlwfpbl"`
					Fzjgysfwfpse        float64 `json:"fzjgysfwfpse"`
					Fzjgybhwjlwjzjtxssr float64 `json:"fzjgybhwjlwjzjtxssr"`
					Fzjgybhwjlwxssr     float64 `json:"fzjgybhwjlwxssr"`
					Fzjgybhwjlwfpse     float64 `json:"fzjgybhwjlwfpse"`
					Fzjgysfwfpbl        float64 `json:"fzjgysfwfpbl"`
					Fzjgmc              string  `json:"fzjgmc"`
					Fzjgysfwxssr        float64 `json:"fzjgysfwxssr"`
					Fzjgysfwjzjtfpse    float64 `json:"fzjgysfwjzjtfpse"`
					Fzjgybhwjlwjzjtfpbl float64 `json:"fzjgybhwjlwjzjtfpbl"`
					FzjgFpbl            float64 `json:"fzjgFpbl"`
					Fzjgxssr            float64 `json:"fzjgxssr"`
				} `json:"hznsqyzzsfpbGridlbVO"`
			} `json:"hznsqyzzsfpbGrid"`
			HznsqyzzsfpbForm struct {
				Zjgysfwjzjtxssr    float64 `json:"zjgysfwjzjtxssr"`
				Yhzh               string  `json:"yhzh"`
				Zjggdfpbl          float64 `json:"zjggdfpbl"`
				Ybtse              float64 `json:"ybtse"`
				Yydz               string  `json:"yydz"`
				Zfjgxssr           float64 `json:"zfjgxssr"`
				Zjgybhwjlwfpse     float64 `json:"zjgybhwjlwfpse"`
				Zjgysfwjzjtfpse    float64 `json:"zjgysfwjzjtfpse"`
				Zjgysfwjzjtfpbl    float64 `json:"zjgysfwjzjtfpbl"`
				Khyh               string  `json:"khyh"`
				Zjgybhwjlwjzjtxssr float64 `json:"zjgybhwjlwjzjtxssr"`
				Zjgybhwjlwxssr     float64 `json:"zjgybhwjlwxssr"`
				Zjgybhwjlwjzjtfpse float64 `json:"zjgybhwjlwjzjtfpse"`
				Zjgysfwxssr        float64 `json:"zjgysfwxssr"`
				Lxdh               string  `json:"lxdh"`
				Zjgysfwfpbl        float64 `json:"zjgysfwfpbl"`
				Fddbrxm            string  `json:"fddbrxm"`
				Fpbl               float64 `json:"fpbl"`
				Zjgfpse            float64 `json:"zjgfpse"`
				Zjgnsrmc           string  `json:"zjgnsrmc"`
				Zjgybhwjlwjzjtfpbl float64 `json:"zjgybhwjlwjzjtfpbl"`
				Zjgysfwfpse        float64 `json:"zjgysfwfpse"`
				Zjgybhwjlwfpbl     float64 `json:"zjgybhwjlwfpbl"`
				Zjgnsrsbh          string  `json:"zjgnsrsbh"`
				Zfjgxsehj          float64 `json:"zfjgxsehj"`
			} `json:"hznsqyzzsfpbForm"`
			Slxx struct {
				SlrDm    string `json:"slrDm"`
				SlswjgDm string `json:"slswjgDm"`
				Slrq     string `json:"slrq"`
			} `json:"slxx"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr_hznsqyzzsfpb"`
		ZzssyyybnsrJyzyfjyxxmxb struct {
			JyzyfjyxxmxGrid struct {
				JyzyfjyxxmxGridlbVO []struct {
					Lxdh    string  `json:"lxdh"`
					Yynzzse float64 `json:"yynzzse"`
					Tbr     string  `json:"tbr"`
					Yljjyl  float64 `json:"yljjyl"`
					Yljje   float64 `json:"yljje"`
					Fddbr   string  `json:"fddbr"`
					Jyqccbh string  `json:"jyqccbh"`
					Zqs     float64 `json:"zqs"`
					Ypxh    string  `json:"ypxh"`
				} `json:"jyzyfjyxxmxGridlbVO"`
			} `json:"jyzyfjyxxmxGrid"`
			Slxx struct {
				SlrDm    string `json:"slrDm"`
				SlswjgDm string `json:"slswjgDm"`
				Slrq     string `json:"slrq"`
			} `json:"slxx"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr_jyzyfjyxxmxb"`
		ZzssyyybnsrHqysqyfzjgcdd struct {
			TrccfhdncpzzsjxseGrid struct {
				TrccfhdncpzzsjxseGridlbVO []struct {
					Dqxshwsl      float64 `json:"dqxshwsl"`
					Pjgmdj        float64 `json:"pjgmdj"`
					Qcpjmj        float64 `json:"qcpjmj"`
					Dqgjncpsl     float64 `json:"dqgjncpsl"`
					Dqmj          float64 `json:"dqmj"`
					Kcl           float64 `json:"kcl"`
					Dqyxdkncpjxse float64 `json:"dqyxdkncpjxse"`
					Cpmc1         string  `json:"cpmc1"`
					Hdddhsl       float64 `json:"hdddhsl"`
					Qckcncpsl     float64 `json:"qckcncpsl"`
					Ewbhxh        int     `json:"ewbhxh"`
					Hyncpmc       string  `json:"hyncpmc"`
				} `json:"trccfhdncpzzsjxseGridlbVO"`
			} `json:"trccfhdncpzzsjxseGrid"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr_hqysqyfzjgcdd"`
		Zzsjmssbmxb struct {
			ZzsjmssbmxbjsxmGrid struct {
				ZzsjmssbmxbjsxmGridlbVO []struct {
					Bqfse    float64 `json:"bqfse"`
					SwsxDm   string  `json:"swsxDm"`
					Qcye     float64 `json:"qcye"`
					Bqsjdjse float64 `json:"bqsjdjse"`
					Bqydjse  float64 `json:"bqydjse"`
					Ewbhxh   int     `json:"ewbhxh"`
					Qmye     float64 `json:"qmye"`
					Hmc      string  `json:"hmc"`
				} `json:"zzsjmssbmxbjsxmGridlbVO"`
			} `json:"zzsjmssbmxbjsxmGrid"`
			ZzsjmssbmxbmsxmGrid struct {
				ZzsjmssbmxbmsxmGridlbVO []struct {
					Kchmsxse    float64 `json:"kchmsxse,omitempty"`
					Bqsjkcje    float64 `json:"bqsjkcje,omitempty"`
					SwsxDm      string  `json:"swsxDm,omitempty"`
					Msxsedyjxse float64 `json:"msxsedyjxse,omitempty"`
					Mzzzsxmxse  float64 `json:"mzzzsxmxse"`
					Ewbhxh      int     `json:"ewbhxh"`
					Mse         float64 `json:"mse,omitempty"`
					Hmc         string  `json:"hmc"`
				} `json:"zzsjmssbmxbmsxmGridlbVO"`
			} `json:"zzsjmssbmxbmsxmGrid"`
		} `json:"zzsjmssbmxb"`
		Zzssyyybnsr02Bqjxsemxb struct {
			BqjxsemxbGrid struct {
				BqjxsemxbGridlbVO []struct {
					Se     float64 `json:"se,omitempty"`
					Xmmc   string  `json:"xmmc"`
					Lc     float64 `json:"lc"`
					Je     float64 `json:"je,omitempty"`
					Ewbhxh int     `json:"ewbhxh"`
					Fs     float64 `json:"fs,omitempty"`
					Hmc    string  `json:"hmc"`
				} `json:"bqjxsemxbGridlbVO"`
			} `json:"bqjxsemxbGrid"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr02_bqjxsemxb"`
		Zzssyyybnsr01Bqxsqkmxb struct {
			BqxsqkmxbGrid struct {
				BqxsqkmxbGridlbVO []struct {
					Nsjctzdxse        float64 `json:"nsjctzdxse,omitempty"`
					WkjfpXxynse       float64 `json:"wkjfpXxynse,omitempty"`
					KjqtfpXse         float64 `json:"kjqtfpXse,omitempty"`
					KjskzzszyfpXxynse float64 `json:"kjskzzszyfpXxynse,omitempty"`
					NsjctzXxynse      float64 `json:"nsjctzXxynse,omitempty"`
					HjXxynse          float64 `json:"hjXxynse,omitempty"`
					KjskzzszyfpXse    float64 `json:"kjskzzszyfpXse,omitempty"`
					Xse               float64 `json:"xse"`
					WkjfpXse          float64 `json:"wkjfpXse,omitempty"`
					Ewbhxh            int     `json:"ewbhxh"`
					KjqtfpXxynse      float64 `json:"kjqtfpXxynse,omitempty"`
					Hmc               string  `json:"hmc"`
					KchXxynse         float64 `json:"kchXxynse,omitempty"`
					Ysfwkcxmbqsjkcje  float64 `json:"ysfwkcxmbqsjkcje,omitempty"`
					Jshj              float64 `json:"jshj,omitempty"`
					KchHsmsxse        float64 `json:"kchHsmsxse,omitempty"`
				} `json:"bqxsqkmxbGridlbVO"`
			} `json:"bqxsqkmxbGrid"`
			Sbbhead struct {
				Nsrsbh string `json:"nsrsbh"`
				Skssqz string `json:"skssqz"`
				Nsrmc  string `json:"nsrmc"`
				Skssqq string `json:"skssqq"`
			} `json:"sbbhead"`
		} `json:"zzssyyybnsr01_bqxsqkmxb"`
		Zzsfjssb struct {
			HznsFjsffpbGrid struct {
				HznsFjsffpbGridlb []struct {
					Fzjgmc     string  `json:"fzjgmc"`
					Fzjgnsrsbh string  `json:"fzjgnsrsbh"`
					Rdpzuuid   string  `json:"rdpzuuid"`
					ZspmDm     string  `json:"zspmDm"`
					Sl1        float64 `json:"sl1"`
					Ybtse      float64 `json:"ybtse"`
					Fzjgdjxh   string  `json:"fzjgdjxh"`
					Jsyj       float64 `json:"jsyj"`
					ZsxmDm     string  `json:"zsxmDm"`
				} `json:"hznsFjsffpbGridlb"`
			} `json:"hznsFjsffpbGrid"`
			FjsxxGrid struct {
				FjsxxGridlb []struct {
					Zzsxejmje     float64 `json:"zzsxejmje"`
					PhjmxzDm      string  `json:"phjmxzDm"`
					SsjmxzDm      string  `json:"ssjmxzDm"`
					Jmse          float64 `json:"jmse"`
					Phjmse        float64 `json:"phjmse"`
					ZspmDm        string  `json:"zspmDm"`
					Ybtse         float64 `json:"ybtse"`
					Jsyj          float64 `json:"jsyj"`
					ZsxmDm        string  `json:"zsxmDm"`
					Bqcjrhxqydmje float64 `json:"bqcjrhxqydmje"`
					CjrhjmxzDm    string  `json:"cjrhjmxzDm"`
					SwsxDm        string  `json:"swsxDm"`
					Zzsmdse       float64 `json:"zzsmdse"`
					Rdpzuuid      string  `json:"rdpzuuid"`
					Yjse1         float64 `json:"yjse1"`
					Phjzbl        float64 `json:"phjzbl"`
					Sl1           float64 `json:"sl1"`
					Sybh          string  `json:"sybh"`
					Zzsldtse      float64 `json:"zzsldtse"`
					PhjmswsxDm    string  `json:"phjmswsxDm"`
					Ynse          float64 `json:"ynse"`
				} `json:"fjsxxGridlb"`
			} `json:"fjsxxGrid"`
			FjsxxForm struct {
				Jzxqldtse         float64 `json:"jzxqldtse"`
				Sqldkdmje         float64 `json:"sqldkdmje"`
				Dqxzldtse         float64 `json:"dqxzldtse"`
				Bchssqq           string  `json:"bchssqq"`
				Dqxztze           float64 `json:"dqxztze"`
				Bqsfsycjyhxqyjzzc string  `json:"bqsfsycjyhxqyjzzc"`
				Syxgmjzzcqssj     string  `json:"syxgmjzzcqssj"`
				Bqsfsyxgmyhzc     string  `json:"bqsfsyxgmyhzc"`
				Syxgmjzzczzsj     string  `json:"syxgmjzzczzsj"`
				Jzxqkdmje         float64 `json:"jzxqkdmje"`
				Bchssqz           string  `json:"bchssqz"`
				Jsyjxgyy          string  `json:"jsyjxgyy"`
				Jsyjxgxz          string  `json:"jsyjxgxz"`
				JzzcsyztDm        string  `json:"jzzcsyztDm"`
				Sqjcldtse         float64 `json:"sqjcldtse"`
			} `json:"fjsxxForm"`
		} `json:"zzsfjssb"`
	} `json:"zzsybsbSbbdxxVO"`
	JmxxGrid struct {
		JmxxGridlb []interface{} `json:"jmxxGridlb"`
	} `json:"jmxxGrid"`
}
