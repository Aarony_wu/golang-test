package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8080")
	if err != nil {
		fmt.Println("打开服务端的连接出错了,", err)
	}

	defer conn.Close()

	go func() {
		str := make([]byte, 4096)
		for {
			n, err := os.Stdin.Read(str)
			if err != nil {
				fmt.Println("获取键盘输入出错了", err)
			}
			conn.Write(str[:n])
		}
	}()
	for {
		buf := make([]byte, 4096)
		n, err := conn.Read(buf)
		if n == 0 {
			fmt.Println("服务器端关闭了连接")
			return
		}
		if err != nil {
			fmt.Println("获取数据出错了", err)
		}
		fmt.Println("获取到的数据为：", string(buf[:n]))
	}
}
