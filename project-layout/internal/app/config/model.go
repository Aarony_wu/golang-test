package config

type AppConfigModel struct {
	Server   AppInfo
	Email    EmailConnection
	Database DataBaseConnection
	Log      LogInfo
}

type AppInfo struct {
	Port         int `json:"Port"`
	ReadTimeout  int `json:"ReadTimeout"`
	WriteTimeout int `json:"WriteTimeout"`
}

type LogInfo struct {
	Level string `json:"Level"`
}

// 数据库连接
type DataBaseConnection struct {
	Host     string `json:"Host"`
	Port     int    `json:"Port"`
	UserName string `json:"UserName"`
	Password int    `json:"Password"`
	DbName   string `json:"DbName"`
	DbType   string `json:"DbType"`
}

type EmailConnection struct {
	Host     string   `json:"Host"`
	Port     int      `json:"Port"`
	UserName string   `json:"UserName"`
	Password int      `json:"Password"`
	IsSSL    bool     `json:"IsSSL"`
	From     string   `json:"From"`
	To       []string `json:"To"`
}

// 基础连接结构体
type BaseConnection struct {
	Host     string `json:"Host"`
	Port     int    `json:"Port"`
	UserName string `json:"UserName"`
	Password int    `json:"Password"`
}
