package main

import (
	log "github.com/sirupsen/logrus"
	"strings"
	"time"
)

func main() {
	tm := time.Now()
	s := strings.ReplaceAll(tm.Format("2006-01-02"), "-", "")
	log.Println(s)
}
