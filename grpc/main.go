package main

import (
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"grpc/interceptor"
	pb2 "grpc/pb"
	"grpc/service"
	"grpc/utils"
	"net"
)

const (
	port = ":52051"
)

func main() {
	/*cert, err := tls.LoadX509KeyPair(
		data.Path("/Users/guirong/go/src/grpc-demo/helloworld/server/server.crt"),
		data.Path("/Users/guirong/go/src/grpc-demo/helloworld/server/server.key"))
	if err != nil {
		log.Fatalf("failed to load key pair: %s", err)
	}*/
	log.Println("开始进入 main 方法")
	// Define customfunc to handle panic
	opts := utils.ErrorUtil().InitCurrentException()
	s := grpc.NewServer(
		//grpc.Creds(credentials.NewServerTLSFromCert(&cert)),
		grpc.ChainUnaryInterceptor(interceptor.UnaryServerInterceptor(opts...)),
		grpc.ChainStreamInterceptor(interceptor.StreamServerInterceptor(opts...)),
	)

	pb2.RegisterGreeterServer(s, &service.GreeterServer{})
	// 玩家连续进行了多次战斗请求，服务器将操作结果响应给玩家
	pb2.RegisterBattleServer(s, &service.BattleServer{})

	listen, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	//不执行这个会抛， server does not support the reflection API
	reflection.Register(s)

	log.Println("Serving gRPC on 0.0.0.0" + port)
	if err := s.Serve(listen); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
