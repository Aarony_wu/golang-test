package main

import "fmt"

func print() {
	for i := 0; i < 8; i++ {
		fmt.Println("print循环了", i)
	}
}

func print1() {
	for i := 0; i < 8; i++ {
		fmt.Println("print1循环了", i)
	}

}

//var cha = make(chan int)
//测试CPU轮询调度
func main() {
	go print()
	go print1()

	for  {
		;
	}
}
