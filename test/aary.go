package main

func main() {
	var s = make([]int, 3)
	s[0] = 1
	s[1] = 2
	println(len(s))
	println(cap(s))

	var s1 = make([]int, 0)
	for i := 0; i < 10; i++ {
		s1 = append(s1, i)
	}

	println(len(s1))
	println(cap(s1))
}
