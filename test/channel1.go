package main

import "fmt"

func main() {
	cha := make(chan int)
	fmt.Println(cha)
	go func() {
		for i := 0; i < 8; i++ {
			fmt.Println("print循环了", i)
			cha <- i
		}
	}()
	for {
		num := <-cha
		fmt.Println("获取到的num", num)
	}
}
