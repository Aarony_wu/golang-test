package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
)

type Person1 struct {
	name      string
	age       int
	flg       bool
	intereset []string
}

func initFunc(p *Person1) {
	p.name = "张三"
	p.age = 19
	p.flg = false
	p.intereset = append(p.intereset, "泡妞")
	p.intereset = append(p.intereset, "测试")
}

func strFunc() {
	str := " I love my word and I love my family too"

	resut := strings.Split(str, " ")
	for _, s := range resut {
		fmt.Println(s)
	}
}

func main() {
	var a int = 0
	num1 := strconv.FormatFloat(float64(a)/100, 'f', 2, 64)
	log.Println("执行完毕")
	log.Println(num1)
	//var p Person1
	//initFunc(&p)

	//fmt.Println("p=", p)

	//strFunc()
}
