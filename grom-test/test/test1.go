package main

import (
	"flag"
	"grom-test/query/test1"
	"grom-test/test/model"
	"grom-test/test/query"
)

func main() {

	flag.Parse()

	u := query.TbSettleSummary

	user := model.TbSettleSummary{ID: 123}
	u.Save(&user).Error()
	querySql := test1.Use(nil, nil)
	point := querySql.DtfDemoPoint
	point.Select(point.AccountID, point.Frozen).Where(point.AccountID.Eq(10))
	querySql.ReplaceDB()
}
