package main

import (
	"fmt"
	"net"
)

//创建服务端
func main() {
	listener, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		fmt.Println("创建监听对象失败了", err)
		return
	}

	fmt.Println("服务器创建监听成功-------等待客户端连接-------")
	cnn, err := listener.Accept()
	if err != nil {
		fmt.Println("listener.Accept() 监听失败", err)
		return
	}
	fmt.Println("服务器已经与客户端成功建立连接--------")
	bur := make([]byte, 4096)
	n, err := cnn.Read(bur)
	if err != nil {
		fmt.Println("获取客户端数据失败", err)
		return
	}
	fmt.Println("读取到客户端的请求数据为：", string(bur[:n]))
	cnn.Write([]byte("我草，你是谁"))
	defer listener.Close()
	defer cnn.Close()
}
