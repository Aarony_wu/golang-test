module protobuf-test

go 1.16

require (
	github.com/golang/glog v1.2.0 // indirect
	github.com/golang/protobuf v1.5.3
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.11.3
	golang.org/x/net v0.16.0
	google.golang.org/genproto/googleapis/api v0.0.0-20231002182017-d307bd883b97
	google.golang.org/grpc v1.60.1
	google.golang.org/protobuf v1.32.0
)
