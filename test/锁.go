package main

import (
	"fmt"
	"sync"
	"time"
)

//建议锁，一个进程只有一个强制锁（只有操作系统用的到）
//上层使用的都是建议锁，就算加了建议锁，直接访问数据还是可以访问的到

var mutex sync.Mutex//互斥锁

func print12(str string) {
	mutex.Lock() //锁的使用
	//fmt.Println(str)
	for _, data := range str {
		fmt.Printf("%c", data)
		time.Sleep(time.Second)
	}
	mutex.Unlock()
}

func person1() {
	print12("hellotest")
}

func person2() {
	print12("worldtest")
}

func main() {
	go person1()
	go person2()
	for {

	}
}
