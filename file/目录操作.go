package main

import (
	"fmt"
	"os"
)

// var path string="/Users/appleli/work/golang/"
func main() {
	fmt.Println("请输入目录")
	var path string
	fmt.Scan(&path)

	dir, err := os.OpenFile(path, os.O_RDONLY, os.ModeDir)
	if err != nil {
		fmt.Println("打开文件夹出错了", err)
	}

	names, err := dir.ReadDir(-1)
	if err != nil {
		fmt.Println("读取文件夹出错了", err)
	}
	for _, name := range names {
		if name.IsDir() {
			fmt.Println(name.Name() + " 这个是一个文件夹")
		} else {
			fmt.Println(name.Name() + " 这个是一个文件")
		}
	}
}
