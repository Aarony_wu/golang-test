package main

import (
	"fmt"
	"net"
	"time"
)

func main() {
	srvAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:8001")
	if err != nil {
		fmt.Println("创建服务器地址失败", err)
		return
	}
	fmt.Println("创建服务器地址成功--------")
	udpConn, err := net.ListenUDP("udp", srvAddr)
	if err != nil {
		fmt.Println("创建服务器监听失败--------")
		return
	}
	defer udpConn.Close()

	buf := make([]byte, 4096)
	for  {
		//在这里会阻塞读取
		n, cliAddr, err := udpConn.ReadFromUDP(buf)
		if err != nil {
			fmt.Println("获取客户端数据失败-------")
			return
		}
		fmt.Printf("获取到客户端 %v 的数据为 %s \n", cliAddr, buf[:n])
		datetime := time.Now().String()

		udpConn.WriteToUDP([]byte(datetime), cliAddr)
	}

}
