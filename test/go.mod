module test

go 1.16

require (
	github.com/aliyun/aliyun-oss-go-sdk v0.0.0-20220324034328-3b6897a270c5
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.7.1
	github.com/gohouse/converter v0.0.3
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sirupsen/logrus v1.9.3
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/time v0.5.0 // indirect
)
