package main

import (
	"log"
	"net"
	"net/rpc"
	"protobuf-test/service"
)

func main() {
	log.Println("测试 protobuf 的 demo")
	rpc.RegisterName("HelloService", new(service.HelloService))
	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error:", err)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("Accept error:", err)
		}
		rpc.ServeConn(conn)
	}

}
