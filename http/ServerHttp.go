package main

import (
	"fmt"
	"net/http"
)

func handler(response http.ResponseWriter, r *http.Request) {
	fmt.Println("客户端的地址为：" + r.RemoteAddr)
	fmt.Println("" + r.Host)
	fmt.Println("" + r.Method)
	fmt.Println("" + r.RequestURI)
	fmt.Println(r.Header)

	response.Write([]byte("hello world!!!!"))

}

func main() {
	http.HandleFunc("/", handler)
	//监听8000
	http.ListenAndServe("127.0.0.1:8000", nil)

}
