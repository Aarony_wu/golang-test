package main

import (
	"fmt"
	"github.com/tealeg/xlsx"
	"time"
)

func main() {
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet1")
	if err != nil {
		fmt.Println(err)
		return
	}
	titleRow := sheet.AddRow()
	titleRow.AddCell().SetValue("数字两个分数")
	titleRow.AddCell().SetValue("时间")

	row := sheet.AddRow()
	cell := row.AddCell()
	cell.SetFloatWithFormat(123.456, "0.00")
//    style, err := f.NewStyle(`{"number_format": 0}`)
	cellDatie := row.AddCell()
	//cellDatie.NumFmt = "yyyy-mm-dd hh:mm:ss"
	// timeStr:=time.Now().Format("2006-01-02 15:04:05")
	//当前时间的字符串，2006-01-02 15:04:05据说是golang的诞生时间，固定写法
	cellDatie.SetValue(time.Now().Format("2006-01-02 15:04:05"))

	row1 := sheet.AddRow()
	cell1 := row1.AddCell()
	cell1.NumFmt = "0.00"
	cell1.SetValue(20.46)

	cellDatie1 := row1.AddCell()
	//cellDatie1.NumFmt = ""
	// time.Now().Format("2023-01-02 15:04:05")
	cellDatie1.SetDateTimeWithFormat(float64(time.Now().Unix()), "yyyy-mm-dd hh:mm:ss")

	err = file.Save("output.xlsx")
	if err != nil {
		fmt.Println(err)
	}
}
