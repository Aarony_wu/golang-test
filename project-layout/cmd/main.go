package main

import (
	log "github.com/sirupsen/logrus"
	"project-layout/internal/app"
	"time"
)

func main() {
	log.Infof("############### 开始执行 main 函数 ################")
	app.InitSystemConfig()
	/*config.NewAppConfigInstance().RegisterChangeEvent(func(config config.AppConfigModel) {
		log.Infof("main -----注册的事件开始执行了-----")
	})*/
	time.Sleep(time.Duration(2) * time.Hour)
}
