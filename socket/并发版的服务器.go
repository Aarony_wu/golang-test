package main

import (
	"fmt"
	"net"
	"strings"
)

func processHandler(conn net.Conn) {
	defer conn.Close()
	fmt.Println("成功建立一个客户端连接---------", conn.RemoteAddr())
	buf := make([]byte, 4096)
	for { //循环接收和发送
		n, err := conn.Read(buf)
		if n == 0 {
			fmt.Println("服务器读获取到客户端关闭连接")
			break
		}
		//这个\n 是因为nc命令会带上一个\n的命令传过来
		if "exit\n" == string(buf[:n]) {
			fmt.Println("服务器判断到客户端请求关闭连接exit")
			return
		}
		if err != nil {
			fmt.Println("获取客户端的数据失败了", err)
			return
		}
		fmt.Println("获取到的客户端数据为：", string(buf[:n]))
		conn.Write([]byte( strings.ToUpper(string(buf[:n]))))
	}
}

func main() {
	listen, err := net.Listen("tcp", "127.0.0.1:8080")
	if err != nil {
		fmt.Println("监听出错了", err)
		return
	}
	defer listen.Close()
	fmt.Println("服务端监听成功------------")
	for {
		conn, err := listen.Accept()
		if err != nil {
			fmt.Println("监听出错了", err)
			return
		}
		//成功建立一个客户端连接
		go processHandler(conn)
	}

}
