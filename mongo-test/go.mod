module mongo-test

go 1.16

require (
	github.com/google/uuid v1.3.0 // indirect
	go.mongodb.org/mongo-driver v1.10.2 // indirect
)
