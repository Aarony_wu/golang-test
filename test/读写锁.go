package main

import (

	"fmt"
	"math/rand"
	"time"
)

func readGo(chan2 <-chan int) {

}

func writeGo(chan1 chan<- int) {
	num := rand.Intn(100)
	chan1 <- num
	fmt.Println("写入go程", num)
	time.Sleep(time.Second * 2)
}

var cha chan int = make(chan int)

//读写锁
func main() {
	quit := make(chan bool)
	for i := 0; i < 9; i++ {
		go readGo(cha)
	}

	for i := 0; i < 9; i++ {
		go writeGo(cha)
	}

	<-quit
}
