package main

import "reflect"

type Person struct {
	name string
}

type PersonCopy struct {
	Person
	NameValue string
}

type PersonDouble struct {
	Person
	NameValue string
	D1        float64
}

func main() {
	p := Person{
		name: "占山",
	}
	pcopy := PersonCopy{
		p, "哈哈",
	}
	pcopy.name = "李四"
	pcopy.NameValue = "hha"

	pcopy1 := PersonDouble{
		p, "哈哈", 1232.12,
	}

	pcopy1.name = "李四"
	pcopy1.NameValue = "hha"
	println(p.name)
	println(pcopy.name)
	println(pcopy1.name)
	t := reflect.TypeOf(pcopy1.D1)
	if t.Name() == "float64" {
		println("得到了当前值的类型")
	}
	//var int int
	//var str string
	//var p *Person
	//fmt.Println(int)
	//fmt.Println("&p",&p)
	//fmt.Printf("T%", str)
	//

	/*maps := map[string]int{"name": 32423, "阿哥": 324234}
	if val, ok := maps["name"]; ok {
		if ok {
			fmt.Println(val)
		} else {
			fmt.Println("找不到该数据")
		}
	}*/

}
