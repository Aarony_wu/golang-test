package base

// BubbleSort 冒泡排序/**
func BubbleSort(arr []int) {
	flag := false
	//外层控制行
	for i := 0; i < len(arr)-1; i++ {
		//内层控制列
		for j := 0; j < len(arr)-1-i; j++ {
			//比较两个相邻元素
			if arr[j] > arr[j+1] {
				//交换数据
				arr[j], arr[j+1] = arr[j+1], arr[j]
				flag = true
			}
		}
		//判断数据是否是有序
		//假如上面的交换没有发生过，那么证明没有继续循环的意义，
		//因为数据本身就是有序的，这种主要是用来处理数据比较有序的情况下的循环
		if !flag {
			return
		} else {
			flag = false
		}
	}
}
