package main

import (
	"fmt"
	"io"
	"os"
)

var path string = "/Users/appleli/work/golang/workspace/test/test.txt"
var writePath string = "/Users/appleli/work/golang/workspace/test/test1.txt"

func main() {
	fin, err := os.Open(path) //打开文件
	if err != nil {
		fmt.Println("打开文件出错了", err)
		return
	}

	fou, err := os.Create(writePath) //创建文件
	if err != nil {
		fmt.Println("打开文件出错了", err)
		return
	}
	buffer := make([]byte, 4096)

	defer fin.Close()
	defer fou.Close()
	for {
		n, err := fin.Read(buffer)
		if err != nil && err != io.EOF {
			fmt.Println("打开文件出错了", err)
			break
		}
		if n == 0 {
			fmt.Println("文件写入完成")
			break
		}
		n1, err := fou.Write(buffer[:n])
		if err != nil {
			fmt.Println("写入文件出错了", err)
		}
		fmt.Println("写入数为", n1)
	}
}
