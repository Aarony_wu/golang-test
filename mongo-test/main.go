package main

import (
	"context"
	"encoding/json"
	"flag"
	"github.com/google/uuid"
	"io/ioutil"
	"log"
	"mongo-test/mongodb"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	User     string
	Pwd      string
	Url      string
	Database string
	Count    int
	lo       int
)
var wg sync.WaitGroup //定义一个同步等待的组

func initData() {
	flag.StringVar(&User, "user", "", "pg user")
	flag.StringVar(&Pwd, "pwd", "", "pg pwd")
	flag.StringVar(&Url, "url", "10.211.55.8:27017", "pg url")
	flag.IntVar(&Count, "count", 100, "pg count")
	flag.StringVar(&Database, "database", "test", "")
	flag.IntVar(&lo, "lo", 2200000000, "")
}

func main() {
	initData()
	//解析定义的监听端口等信息
	flag.Parse()
	ctx := context.Background()
	//连接数据库
	if User == "" {
		err := mongodb.NewMongoDB(ctx, "mongodb://"+Url+"/admin")
		log.Println("---------没有带 user 账户-------")
		if err != nil {
			log.Fatalf("%s", err)
			return
		}
	} else {
		log.Println("---------账户信息-------" + User + ":" + Pwd + "|" + Url)
		appUrl := "mongodb://" + User + ":" + Pwd + "@" + Url + "/admin"
		log.Println(appUrl)
		err := mongodb.NewMongoDB(ctx, appUrl)
		if err != nil {
			log.Fatalf("%s", err)
			return
		}
	}
	//insertTestData(ctx)
	insertFileTestData(ctx)
	//wg.Wait() //阻塞直到所有任务完成
	log.Println("--------执行完成所有任务---------")
}

func writeData2Csv(){

}
//根据文件初始化脚本
func insertFileTestData(ctx context.Context) {
	content, err := ioutil.ReadFile("./test.json")
	if err != nil {
		log.Fatal("Error when opening file: ", err)
	}

	// Now let's unmarshall the data into `payload`
	var payload mongodb.BigData
	err = json.Unmarshal(content, &payload)
	if err != nil {
		log.Fatal("Error during Unmarshal(): ", err)
	}
	mgdb := mongodb.NewMGDB(Database, "sb_sbb")
	for i := 0; i < Count; i++ {
		log.Println("-----开始执行循环-----")
		inserBigDta(ctx, payload, mgdb, lo)
	}
}

func inserBigDta(ctx context.Context, payload mongodb.BigData, mgdb *mongodb.MongoDB, lo int) {
	var tests3 []mongodb.BigData
	for i := 1; i < 100; i++ {
		lo = lo + 1
		t2 := payload
		t3 := &t2 //改动地方
		t3.Sbuuid = strings.ReplaceAll(uuid.New().String(), "-", "")
		t3.SBSBbcTjqtxxVO.Djxh= "D" + strconv.Itoa(lo)
		tests3 = append(tests3, *t3)
		/*jsonStu, err := json.Marshal(t3)
		if err != nil {
			fmt.Println("生成json字符串错误")
		}*/
		//log.Println(string(jsonStu))
		//log.Println(json.Marshal(t3))
	}
	mgdb.InsertDataMultiple(ctx, tests3)
}

func insertTestData(ctx context.Context) {
	//设置使用的库和表
	mgdb := mongodb.NewMGDB(Database, "sb_sbb_zb")
	mgdb1 := mongodb.NewMGDB(Database, "sb_sbb_fb")
	mgdb2 := mongodb.NewMGDB(Database, "sb_sbjz_flb")
	mgdb3 := mongodb.NewMGDB(Database, "sb_sbjz_mxb")
	id := "t_szzh_sxcj_" + strconv.Itoa(lo)
	zd := &mongodb.Sb_sbb_zb{
		Id:      id,
		Xse:     1580934.0,
		Yshwxse: 2323.0,
		Yslwxse: 23423.0,
		Xxse:    2342.0,
		Jxse:    2342,
		Qcwjsw:  2432,
		Yptse:   23423,
		Sbflzl1: struct {
			Zsxm     string `json:"zsxm"`
			Zsxmjcxx string `json:"zsxmjcxx"`
		}{
			Zsxm: "01", Zsxmjcxx: "基础信息：数据",
		},
		Sbflzl2: struct {
			Zsxm     string `json:"zsxm"`
			Zsxmjcxx string `json:"zsxmjcxx"`
		}{Zsxm: "02", Zsxmjcxx: "基础信息：结构"},
		Sbflzl3: struct {
			Zsxm     string `json:"zsxm"`
			Zsxmjcxx string `json:"zsxmjcxx"`
		}{Zsxm: "03", Zsxmjcxx: "基础信息：附加"},
		Jszmxx: []struct {
			Jzmxxuuid string `json:"jzmxxuuid"`
		}{
			{Jzmxxuuid: uuid.New().String()},
		},
	}
	for i := 0; i < Count; i++ {
		log.Println("-----开始执行循环-----")
		if i != 0 && i%100 == 0 {
			log.Println("------开始进入休眠-----10秒-----")
			time.Sleep(time.Duration(10) * time.Second)
		}
		//wg.Add(1) //添加一个计数
		//go insertToMongo(lo, zd, mgdb, ctx, mgdb1, mgdb2, mgdb3)
		insertToMongo(zd, mgdb, ctx, mgdb1, mgdb2, mgdb3)
	}
}

func insertToMongo(zd *mongodb.Sb_sbb_zb, mgdb *mongodb.MongoDB, ctx context.Context, mgdb1 *mongodb.MongoDB, mgdb2 *mongodb.MongoDB, mgdb3 *mongodb.MongoDB) {
	//插入多条
	var tests []mongodb.Sb_sbb_zb
	var tests1 []mongodb.Sb_sbb_fb
	var tests2 []mongodb.Sb_sbjz_flb
	var tests3 []mongodb.Sb_sbjz_mxb
	for i := 1; i < 1000; i++ {
		lo = lo + 1
		id := "t_szzh_sxcj_" + strconv.Itoa(lo)
		tmp := *zd
		stud2 := &tmp
		stud2.Id = id
		tests = append(tests, *stud2)

		fb := mongodb.Sb_sbb_fb{
			Sbuuid:     stud2.Id,
			Zsxmdm:     "01",
			Zsxmzpxse:  23432423,
			Zsxmzpxxse: 432423,
			Mdtsxx: []struct {
				Tssx string  `json:"tssx"`
				Tsse float64 `json:"tsse"`
			}{
				{Tsse: 23432,
					Tssx: "事项一"},
			},
		}
		fb1 := mongodb.Sb_sbb_fb{
			Sbuuid:     stud2.Id,
			Zsxmdm:     "01",
			Zsxmzpxse:  234324223,
			Zsxmzpxxse: 432423,
			Mdtsxx: []struct {
				Tssx string  `json:"tssx"`
				Tsse float64 `json:"tsse"`
			}{
				{Tsse: 23432,
					Tssx: "事项一"},
			},
		}
		fb2 := mongodb.Sb_sbb_fb{
			Sbuuid:     stud2.Id,
			Zsxmdm:     "02",
			Zsxmzpxse:  23432433,
			Zsxmzpxxse: 432423,
			Mdtsxx: []struct {
				Tssx string  `json:"tssx"`
				Tsse float64 `json:"tsse"`
			}{
				{Tsse: 23432,
					Tssx: "事项一"},
			},
		}
		tests1 = append(tests1, fb, fb1, fb2)

		flb := mongodb.Sb_sbjz_flb{
			Sbuuid:   stud2.Id,
			Zsxmxx:   "程平友",
			Flzluuid: "dafa",
			Jzkmdm:   "10001",
			Jzkmmc:   "科目一",
		}
		flb1 := mongodb.Sb_sbjz_flb{
			Sbuuid:   stud2.Id,
			Zsxmxx:   "程平友",
			Flzluuid: "dafa",
			Jzkmdm:   "10002",
			Jzkmmc:   "科目一",
		}
		flb2 := mongodb.Sb_sbjz_flb{
			Sbuuid:   stud2.Id,
			Zsxmxx:   "程平友",
			Flzluuid: "dafa",
			Jzkmdm:   "10003",
			Jzkmmc:   "科目一",
		}
		tests2 = append(tests2, flb, flb1, flb2)

		mxb := mongodb.Sb_sbjz_mxb{
			Sbuuid:   stud2.Id,
			Fphm:     "100000",
			Xh:       1,
			Kprq:     "2022-09-09",
			FppzDm:   "100000",
			TdyslxDm: "03",
		}
		mxb1 := mongodb.Sb_sbjz_mxb{
			Sbuuid:   stud2.Id,
			Fphm:     "100001",
			Xh:       1,
			Kprq:     "2022-09-09",
			FppzDm:   "100001",
			TdyslxDm: "03",
		}
		mxb2 := mongodb.Sb_sbjz_mxb{
			Sbuuid:   stud2.Id,
			Fphm:     "100002",
			Xh:       1,
			Kprq:     "2022-09-09",
			FppzDm:   "100002",
			TdyslxDm: "03",
		}
		tests3 = append(tests3, mxb, mxb1, mxb2)
	}
	mgdb.InsertMultiple(ctx, tests)
	log.Printf("插入多条记录Sb_sbb_zb成功")
	mgdb1.InsertMultiple1(ctx, tests1)
	log.Printf("插入多条Sb_sbb_fb记录成功")
	mgdb2.InsertMultiple2(ctx, tests2)
	log.Printf("插入多条Sb_sbjz_flb记录成功")
	mgdb3.InsertMultiple3(ctx, tests3)
	log.Printf("插入多条Sb_sbjz_mxb记录成功")
	//wg.Done()
}
