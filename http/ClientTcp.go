package main

import (
	"fmt"
	"net"
)

func main() {
	//监听客户端
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		fmt.Println("出错了", err)
		return
	}
	defer conn.Close()
	//http使用的是\r\n 来做换行的
	// 模拟浏览器，组织一个最简单的请求报文。包含请求行，请求头，空行即可。
	requestHttpHeader := "GET /hello HTTP/1.1\r\nHost:127.0.0.1:8000\r\n\r\n"
	conn.Write([]byte(requestHttpHeader))

	buf := make([]byte, 4096)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("读取文件失败了---------")
		return
	}

	fmt.Println("获取到服务器返回的数据为："+string(buf[:n]))
}
