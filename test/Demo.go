package main

import "fmt"

func main() {
	test1()
	test()
}

func test1() {
	// 声明第一个包含3 个元素的指向字符串的指针数组
	var array1 [3]*string
	// 声明第二个包含3 个元素的指向字符串的指针数组
	// 使用字符串指针初始化这个数组
	array2 := [3]*string{new(string), new(string), new(string)}
	// 使用颜色为每个元素赋值
	*array2[0] = "Red"
	*array2[1] = "Blue"
	*array2[2] = "Green"
	// 将array2 复制给array1
	array1 = array2
	*array1[2] = "李四"
	fmt.Println("指针类型，数组 1：" + *array1[2])
	fmt.Println("指针类型，数组 2：" + *array2[2])
}

func test() {
	// 声明第一个包含5 个元素的字符串数组
	var array1 [5]string
	// 声明第二个包含5 个元素的字符串数组
	// 用颜色初始化数组
	array2 := [5]string{"Red", "Blue", "Green", "Yellow", "Pink"}
	// 把array2 的值复制到array1
	array1 = array2
	array1[2] = "张山"
	fmt.Println("值类型，数组 1：" + array1[2])
	fmt.Println("值类型，数组 2：" + array2[2])
}
