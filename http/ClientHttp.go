package main

import (
	"fmt"
	"net/http"
)

func main() {

	response, err := http.Get("127.0.0.1:8000/hello")
	if err != nil {
		fmt.Println("获取服务器的数据失败了", err)
		return
	}
	defer response.Body.Close()

	fmt.Println("Status=", response.Status)
	fmt.Println("StatusCode=", response.StatusCode)
	fmt.Println("Header=", response.Header)
	fmt.Println("Body=", response.Body)

	buf := make([]byte, 4096)
	var result string
	for {
		n, err := response.Body.Read(buf)
		if n == 0 {
			fmt.Println("获取服务器的数据包出错了", err)
			return
		}
		result += string(buf[:n])
	}

	fmt.Println("result=", result)
}
