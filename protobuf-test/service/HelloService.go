package service

import "log"

type HelloService struct {
}

func (p *HelloService) Hello(request string, reply *string) error {
	log.Println(request)
	*reply = "hello:" + request
	return nil
}
