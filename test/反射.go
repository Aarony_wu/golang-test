package main

import (
	"fmt"
	"reflect"
)

type PersonDouble1 struct {
	Name      string
	NameValue string
	D1        float64
	Age       int32
	Bitch     float32
	Date      string
	D2        string
}

func main() {
	pcopy1 := PersonDouble1{
		"占山", "哈哈", 1232.12, 12, 123.23, "2023-01-01", "23",
	}
	//创建接收任何值的数组
	fields := make([]interface{}, 0)
	fields = append(fields, pcopy1.D1)
	fields = append(fields, pcopy1.Name)
	fields = append(fields, pcopy1.Age)
	fields = append(fields, pcopy1.Bitch)
	fields = append(fields, pcopy1.Date)
	fields = append(fields, pcopy1.D2)

	for i, field := range fields {
		println(i)
		println(fmt.Sprintf("%v", field))
		t := reflect.TypeOf(field)
		if t.Name() == "float64" {
			println("得到了当前值的类型float64")
		}
		if t.Name() == "string" {
			println("得到了当前值的类型string")
		}
		if t.Name() == "int32" {
			println("得到了当前值的类型int32")
		}
		if t.Name() == "float32" {
			println("得到了当前值的类型float32")
		}
	}
}
