package config

import (
	"encoding/json"
	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"reflect"
)

type AppConfig struct {
}

var (
	model AppConfigModel
	//通知事件集合
	changes []func(config AppConfigModel)
)

func NewAppConfigInstance() *AppConfig {
	return &AppConfig{}
}

func (a *AppConfig) RegisterChangeEvent(change func(config AppConfigModel)) {
	changes = append(changes, change)
}

// 初始化配置文件
func (a *AppConfig) initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("./configs")
	if err := viper.ReadInConfig(); err != nil {
		log.Panicf("viper read config err: %v", err.Error())
	}

	viper.AutomaticEnv()
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		a.reloadConfig()
	})
	viper.Unmarshal(&model)
	//默认调用一次
	a.doChanageNotify()
	log.Infof(viper.GetString("COMMON.SERVERNAME"))
}

// 获取全局配置
func (a *AppConfig) GetAppConfig() AppConfigModel {
	return model
}

// 赋值
func (a *AppConfig) SetAppConfig(key, value string) {
	// 通过反射修改字段值
	v := reflect.ValueOf(&model).Elem()
	field := v.FieldByName(key)
	if field.IsValid() && field.CanSet() {
		field.SetString(value)
	}
}

// 重新加载配置
func (a *AppConfig) reloadConfig() {
	log.Infof("-----重新加载配置文件-----")
	viper.Unmarshal(&model)
	bytes, err := json.Marshal(model)
	if err != nil {
		log.Errorf("出错了，err=%s", err)
	}
	log.Infof(string(bytes))
	a.doChanageNotify()
}

func (a *AppConfig) doChanageNotify() {
	for _, change := range changes {
		change(a.GetAppConfig())
	}
}
