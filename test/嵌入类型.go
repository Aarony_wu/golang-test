package main

import "fmt"

type Person struct {
	Name string
}

type EmployeeRef struct {
	*Person
	EmployeeID int
}

type EmployeeVal struct {
	Person
	EmployeeID int
}

func main() {
	/**  -------------------不管是引用还是值类型都会改变值--------------------
	 */

	// 嵌入类型为引用类型
	empRef := EmployeeRef{&Person{"John"}, 123}
	empRef.Name = "Doe"
	fmt.Println(empRef.Name)        // 输出 Doe
	fmt.Println(empRef.Person.Name) // 输出 Doe

	// 嵌入类型为值类型
	empVal := EmployeeVal{Person{"John"}, 123}
	empVal.Name = "Doe"
	fmt.Println(empVal.Name)        // 输出 Doe
	fmt.Println(empVal.Person.Name) // 输出 Doe
}
